import React, { Component } from 'react';
import SwipeableViews from 'react-swipeable-views';
import './swiper.css';

const slideCount = 10;

export default class Swiper extends Component{
    handleLastSlide(index) {
        let isLastSlide = index === slideCount - 1;
        this.props.finish(isLastSlide);
    }
    render() {
        let slide = [];
        let endClassName = 'swiper-slide swiper-slide-end swiper-slide-end-0'+this.props.correctAnswer;
        
        if (this.props.thumb) {
            slide.push(<div key={0} className={endClassName}></div>);
        } else {
            slide.push(<div key={0} className="swiper-slide swiper-slide-start"></div>);
            for (let i=1; i < slideCount - 1; i++) {
                let slideClassName = i % 2 === 0 ? "swiper-slide slide-odd" : "swiper-slide slide-even";  
                slide.push(<div key={i} className={slideClassName}></div>);
            }
            slide.push(<div key={slideCount} className={endClassName}></div>);
        }
        return (
            <SwipeableViews disabled={this.props.disabled} className="swiper" onChangeIndex={this.handleLastSlide.bind(this)}>
                {slide}
            </SwipeableViews>
        )
    }
}