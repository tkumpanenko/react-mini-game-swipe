const opt = {
    question: 'What’s at the fingertip?',
    answers: [
        'Nail polish',
        'Bubble gum',
        'Dog licking'
    ]
}

export default class Store {

    constructor(){
        this.correctId = 0; 
        this.shake();
    }

    shake() {
        this.correctId = Math.floor(Math.random() * opt.answers.length);
    }

    get correctAnswer() {
        return this.correctId;
    }

    get answers() {
        return opt.answers;
    }

    get question() {
        return opt.question;
    }

}