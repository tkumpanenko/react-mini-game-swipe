import React, { Component } from 'react';
import './answers.css';

export default class Answers extends Component{
    answerClick(index) {
        if (this.props.handleAnswer) {
            this.props.handleAnswer(index);
        }
    }
    render() {
        let wrapperClass = this.props.resultClass ?  "answers-wrapper " + this.props.resultClass: "answers-wrapper";
        return(
           <div className={wrapperClass}>
            <ul>
                {this.props.answers.map((answer, index) => <li key={index} onClick={() => {this.answerClick(index)}} >{answer}</li>)}
            </ul>  
           </div> 
        )
    }
}