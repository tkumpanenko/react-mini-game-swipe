import React, { Component } from 'react';
import './swiper.css';
import swipeImg from '../assets/swipe-right.png';

export default class SwipeIcon extends Component{
    render() {
        return (
            <div className="swipe-icon">
                <img src={swipeImg} alt="swipe right"/>
                SWIPE TO FIND OUT
            </div>
        )
    }
}