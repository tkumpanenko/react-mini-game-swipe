import React, { Component } from 'react';
import type { Output } from '../Types';

import { Button } from 'aq-miniapp';

import Question from '../../components/Question';
import Answers from '../../components/Answers';
import Swiper from '../../components/Swiper';

// Import CSS here
import '../css/View3.css';

type Props = {
  output: Output
}

export class View3 extends Component {
  props: Props;

  render() {
    return (
        <div className="viewContainer justifyCenter">
            <Question question={this.props.output.titleText}/>
            <Swiper correctAnswer={this.props.output.correctAnswer} disabled={true} thumb={true}/>
            <div className="footer">
              <Answers answers={[this.props.output.answer]} resultClass={this.props.output.answerClass} />
              <Button className="start-button" title="Next" onClick={this.props.onClick}/>
            </div>
        </div>
    )
  }
}
