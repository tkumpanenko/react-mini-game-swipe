// @flow
import React, { Component } from 'react';
import type { Output } from '../Types';

// Import component to be developed as required by specs document here
import Store from '../../components/Store';
import Swiper from '../../components/Swiper';
import SwipeIcon from '../../components/SwipeIcon';
import Question from '../../components/Question';
import Answers from '../../components/Answers';

/* Import Assets as required by specs document
ex.
import asset from '../../assets/asset.png';
*/
// Import CSS here
import '../css/View2.css';

/* Define constants here

ex.
const MY_CONSTANT = 42;
*/

export type Props = {
  onClick: (Output) => void
};

export class View2 extends Component {

  state: {
    output: Output
  }

  constructor(props: Props){
    super(props);
    
    this.state = {
      currentStep: 0,//0: not started video, 1: playing video, 2: finished video
      output: {},
      answerId:null,
      swipeDisabled: true,
      store: null
    }

  }
  handleAnswer(id){
    let store = this.state.store;
    this.setState({store, swipeDisabled: false, answerId: id, currentStep: 1});
  }

  componentWillMount(){
    let store = new Store();
    this.setState({store});
  }

  finish(isFinished) {
    if (isFinished) {
      let won = (this.state.store.correctAnswer === this.state.answerId);
      let output =  {
        titleText: won ? 'Correct answer!' : 'Wrong Answer',
        answer: this.state.store.answers[this.state.answerId],
        answerClass: won ? 'correct' : 'wrong',
        correctAnswer: this.state.store.correctAnswer
      }
      setTimeout(()=>{
        this.props.onClick(output);
        this.setState({swipeDisabled: true, currentStep: 2});
      }, 500);
    }
  }

  render() {
    let store = this.state.store;
    let footer = <Answers answers={store.answers} handleAnswer={this.handleAnswer.bind(this)} />;
    
    let question = store.question;
    if (this.state.currentStep === 0) {
      footer = <Answers answers={store.answers} handleAnswer={this.handleAnswer.bind(this)} />;
    } else {
      footer = (<SwipeIcon />);
    } 
    return (
        <div className="viewContainer justifyCenter">
            <Question question={question}/>
            <Swiper correctAnswer={store.correctAnswer} disabled={this.state.swipeDisabled} finish={this.finish.bind(this)}/>
            <div className="footer">
              {footer}
            </div>
            
        </div>
      )
    }

  }
